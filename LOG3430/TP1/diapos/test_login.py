import sqlite3
from unittest.mock import patch
import unittest
import os
from io import StringIO
from diapos import login
from enonce.DAOs import ContactDAO
from enonce.models import Contact


class TestLogin(unittest.TestCase):
    def setUp(self):
        self.db_file = 'yourdb.db'
        self.contactDAO = ContactDAO(self.db_file)
        self.contactDAO.init_db()

        self.firstContact = Contact(1, "First", "Contact", "514-000-0000", "firstContact@polymtl.ca", 1, 2020)
        self.secondContact = Contact(2, "second", "Contact", "514-000-0001", "secondContact@polymtl.ca", 1, 2020)
        self.thirdContact = Contact(3, "Third", "Contact", "514-000-0002", "thirdContact@polymtl.ca", 1, 2020)

        self.contactDAO.add(self.firstContact)
        self.contactDAO.add(self.secondContact)
        self.contactDAO.add(self.thirdContact)

    def tearDown(self):
        os.remove(self.db_file)

    @patch('builtins.input')
    def test_login_shoud_tell_user_when_username_or_password_wrong_and_print_goodbye_msg_when_user_choose_to_quit_with_n(self, mock_inputs):
        mock_inputs.side_effect = ['Loto@polymtl.ca', '514-666-6666', 'n']
        with patch('sys.stdout', new=StringIO()) as fake_out:
            login.login()
            let = fake_out.getvalue()
            self.assertEqual(let, 'username ou password erroné !!!\nAu revoir ....\n')

    @patch('builtins.input')
    def test_login_shoud_tell_user_when_username_or_password_wrong_and_print_goodbye_msg_when_user_choose_to_quit_with_arobase(self,
                                                                                                                  mock_inputs):
        mock_inputs.side_effect = ['Loto@polymtl.ca', '514-666-6666', '@']
        with patch('sys.stdout', new=StringIO()) as fake_out:
            login.login()
            let = fake_out.getvalue()
            self.assertEqual(let, 'username ou password erroné !!!\nAu revoir ....\n')

    @patch('builtins.input')
    def test_login_should_ask_user_new_informations_when_password_or_username_are_wrong(self, mock_inputs):
            mock_inputs.side_effect = ['Loto@polymtl.ca', '514-666-6666', 'o', 'firstContact@polymtl.ca', '514-000-0000', 'n']
            with patch('sys.stdout', new=StringIO()) as fake_out:
                login.login()
                let = fake_out.getvalue()
                self.assertEqual(let, 'username ou password erroné !!!\nBienvenue Contact\n')

    def tearDown(self):
        os.remove(self.db_file)


if __name__ == '__main__':
    unittest.main()
