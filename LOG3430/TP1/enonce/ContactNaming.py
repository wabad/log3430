class Contact:

    def __init__(self, first_name, last_name, pay):
        self.first = first_name
        self.last_name = last_name
        self.pay = pay

    def email(self):
        return self.first + '.' + self.last_name +'@' + 'polymtl.ca'

    def fullname(self):
        return  self.first + ' ' + self.last_name

    def apply_raise(self):
        self.pay = self.pay + 0.05*self.pay
