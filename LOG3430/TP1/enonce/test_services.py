import unittest
from unittest import mock
from unittest.mock import Mock, patch
from enonce import services
from enonce.DAOs import ContactDAO
from .services import *
from datetime import datetime


# To complete...
class TestContactService(unittest.TestCase):

    def setUp(self):
        self.contactDAO = Mock()
        self.contactService = ContactService(self.contactDAO)

    def test_when_contact_is_created_updated_should_be_True(self):
        self.contactDAO.add.return_value = 1
        self.contactDAO.get_by_names.return_value = None
        contact = self.contactService.create_contact('Houssem', 'Ben Braiek', '123-456-7891', 'houssem.bb@gmail.com')
        self.assertTrue(contact.updated)

    @mock.patch('enonce.services.getNow')
    def test_when_contact_is_created_updated_date_should_be_now(self, now_mock):
        faketime = datetime.now()
        services.getNow.return_value = faketime
        self.contactDAO.get_by_names.return_value = None
        contact = self.contactService.create_contact('First', 'Contact', '514', 'Mail')
        self.assertEqual(faketime.timestamp(), contact.updated_date)

    def test_when_contact_is_created_and_DAO_get_by_names_returns_contact_it_should_raise_AlreadyExistedItem(self):
        self.contactDAO.get_by_names.return_value = Contact(1, "first", "Contact", "514-0809-3333", "first@Contact.ca",
                                                            True, 2020)

        self.assertRaises(AlreadyExistedItem, self.contactService.create_contact, "first", "Contact", "514-0809-3333",
                          "first@Contact.ca")

    def test_when_contact_is_changed_updated_should_be_True(self):
        contact = self.contactService.update_contact(1, "UpdatedFirst", "Contact", "514-0809-3333",
                                                     "updatedFirst@contact.ca")
        self.assertTrue(contact.updated)

    def test_when_contact_is_changed_updated_date_should_be_now(self):
        contact = self.contactService.update_contact(1, "UpdatedFirst", "Contact", "514-0809-3333",
                                                     "updatedFirst@contact.ca")
        faketime = datetime.now()
        services.getNow.return_value = faketime
        self.contactDAO.get_by_names.return_value = None
        contact = self.contactService.update_contact(1, 'First', 'Contact', '514', 'Mail')
        self.assertEqual(faketime.timestamp(), contact.updated_date)

    def test_when_contact_is_changed_and_DAO_update_returns_zero_it_should_raise_UndefinedID(self):
        self.contactDAO.update.return_value = 0
        self.assertRaises(UndefinedID, self.contactService.update_contact, 1, "first", "Contact", "514-0809-3333",
                          "first@Contact.ca")

    def test_when_retrieve_contact_is_called_with_id_and_DAO_get_by_id_should_be_called(self):
        self.contactDAO.get_by_names.return_value = None
        contact = self.contactService.create_contact("first", "Contact", "514-0809-3333", "first@contact.ca")
        self.contactService.retrieve_contact(1)
        self.assertTrue(self.contactDAO.get_by_id.called)

    def test_when_retrieve_contact_is_called_with_names_and_DAO_get_by_names_should_be_called(self):
        self.contactService.update_contact(9, "second", "Contact", "514-0809-3333", "second@Contact.ca")
        self.contactService.retrieve_contact(None, "second", "Contact")
        self.assertTrue(self.contactDAO.get_by_names.called)

    def test_when_retrieve_contact_is_called_with_id_and_DAO_returns_None_it_should_raise_UndefinedID(self):
        self.contactService.update_contact(1, "second", "Contact", "514-0809-3333", "second@Contact.ca")
        self.contactDAO.get_by_id.return_value = None
        self.assertRaises(UndefinedID, self.contactService.retrieve_contact, 1, "second", "Contact")

    def test_when_retrieve_contact_is_called_with_names_and_DAO_returns_None_it_should_raise_NotExistedItem(self):
        self.contactService.update_contact(1, "second", "Contact", "514-0809-3333", "second@Contact.ca")
        self.contactDAO.get_by_names.return_value = None
        self.assertRaises(NotExistedItem, self.contactService.retrieve_contact, None, "second", "Contact")

    def test_when_delete_contact_is_called_with_id_and_DAO_delete_by_id_should_be_called(self):
        self.contactService.update_contact(9, "second", "Contact", "514-0809-3333", "second@Contact.ca")
        self.contactService.delete_contact(9, "second", "Contact")
        self.assertTrue(self.contactDAO.delete_by_id.called)

    def test_when_delete_contact_is_called_with_names_and_DAO_delete_by_names_should_be_called(self):
        self.contactService.update_contact(9, "second", "Contact", "514-0809-3333", "second@Contact.ca")
        self.contactService.delete_contact(None, "second", "Contact")
        self.assertTrue(self.contactDAO.delete_by_names.called)

    def test_when_delete_contact_is_called_with_id_and_DAO_delete_by_id_returns_zero_it_should_raise_UndefinedID(self):
        self.contactService.update_contact(9, "second", "Contact", "514-0809-3333", "second@Contact.ca")
        self.contactDAO.delete_by_id.return_value = 0
        self.assertRaises(UndefinedID, self.contactService.delete_contact, 9, "second", "Contact")

    def test_when_retrieve_contact_is_called_with_names_and_DAO_delete_by_names_returns_zero_it_should_raise_NotExistedItem(
            self):
        self.contactService.update_contact(9, "second", "Contact", "514-0809-3333", "second@Contact.ca")
        self.contactDAO.delete_by_names.return_value = 0
        self.assertRaises(NotExistedItem, self.contactService.delete_contact, None, "second", "Contact")

    @patch('enonce.services.getDeltaDays')
    def test_when_verify_contact_status_and_active_contacts_contacts_deltaTime_great_than_1095_should_desactivate_them(
            self, mock):
        # Create a database for test
        self.db_file = 'temp.db'
        self.contactDAO = ContactDAO(self.db_file)
        self.contactDAO.init_db()
        self.contactService = ContactService(self.contactDAO)

        services.getDeltaDays.return_value = 2000

        firstContact = Contact(1, "first", "Contact", "514-010-0000", "firstContact@polymtl.ca", 1, 2020)
        secondContact = Contact(2, "second", "Contact", "514-020-0001", "secondContact@polymtl.ca", 1, 2020)
        thirdContact = Contact(3, "third", "Contact", "514-020-0001", "thirdContact@polymtl.ca", 0, 2020)
        fourthContact = Contact(4, "fourth", "Contact", "514-020-0001", "fourthContact@polymtl.ca", 0, 2020)
        self.contactDAO.add(firstContact)
        self.contactDAO.add(secondContact)
        self.contactDAO.add(thirdContact)
        self.contactDAO.add(fourthContact)

        self.contactService.verify_contacts_status()

        self.assertTrue(mock.called)
        self.assertEqual(self.contactDAO.get_by_id(1).updated, 0)
        self.assertEqual(self.contactDAO.get_by_id(2).updated, 0)

        self.assertEqual(self.contactDAO.get_by_id(3).updated, 0)
        self.assertEqual(self.contactDAO.get_by_id(4).updated, 0)

        # remove the database
        os.remove(self.db_file)

    @patch('enonce.services.getDeltaDays')
    def test_when_verify_contact_status_and_active_contacts_deltaTime_less_than_1095_should_not_desactivate_them(
            self, mock):
        # Create a database for test
        self.db_file = 'temp.db'
        self.contactDAO = ContactDAO(self.db_file)
        self.contactDAO.init_db()
        self.contactService = ContactService(self.contactDAO)

        services.getDeltaDays.return_value = 30

        firstContact = Contact(1, "First", "Contact", "514-010-0000", "firstContact@polymtl.ca", 1, 2020)
        secondContact = Contact(2, "second", "Contact", "514-020-0001", "secondContact@polymtl.ca", 1, 2020)
        thirdContact = Contact(3, "third", "Contact", "514-020-0001", "thirdContact@polymtl.ca", 0, 2020)
        fourthContact = Contact(4, "fourth", "Contact", "514-020-0001", "fourthContact@polymtl.ca", 0, 2020)

        self.contactDAO.add(firstContact)
        self.contactDAO.add(secondContact)
        self.contactDAO.add(thirdContact)
        self.contactDAO.add(fourthContact)

        self.contactService.verify_contacts_status()

        self.assertTrue(mock.called)
        self.assertEqual(self.contactDAO.get_by_id(1).updated, 1)
        self.assertEqual(self.contactDAO.get_by_id(2).updated, 1)

        self.assertEqual(self.contactDAO.get_by_id(3).updated, 0)
        self.assertEqual(self.contactDAO.get_by_id(4).updated, 0)

        # remove the database
        os.remove(self.db_file)

if __name__ == '__main__':
    unittest.main()
