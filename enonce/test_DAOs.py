import sqlite3
import unittest.mock
import os
from DAOs import ContactDAO
from models import Contact
from datetime import datetime


def areContactsEquals(first, second):
    return first.first_name == second.first_name and first.last_name == second.last_name and \
           first.mail == second.mail and first.phone == second.phone and first.updated == second.updated and \
           first.updated_date == second.updated_date


def isConctatInList(list, researchedContact):
    for contact in list:
        if areContactsEquals(researchedContact, contact):
            return True
    return False


# To complete...
class TestContactDAO(unittest.TestCase):

    def setUp(self):
        self.db_file = 'temp.db'
        self.contactDAO = ContactDAO(self.db_file)
        self.contactDAO.init_db()

        self.firstContact = Contact(1, "First", "Contact", "514-000-0000", "firstContact@polymtl.ca", 1, 2020)
        self.secondContact = Contact(2, "second", "Contact", "514-000-0001", "secondContact@polymtl.ca", 1, 2020)
        self.thirdContact = Contact(3, "Third", "Contact", "514-000-0002", "thirdContact@polymtl.ca", 1, 2020)
        self.fourthContact = Contact(4, "Fourth", "Contact", "514-000-0002", "fourthContact@polymtl.ca", 1, 2020)
        self.fifthContact = Contact(5, "Fifth", "Contact", "514-000-0002", "fithContact@polymtl.ca", 1, 2020)
        self.sixthContact = Contact(6, "sixth", "Contact", "514-000-0002", "sixthContact@polymtl.ca", 1, 2020)
        self.seventhContact = Contact(7, "Seventh", "Contact", "514-000-0002", "seventhContact@polymtl.ca", 1, 2020)
        self.eighthContact = Contact(8, "Eighth", "Contact", "514-000-0002", "eighthContact@polymtl.ca", 1, 2020)

    def tearDown(self):
        os.remove(self.db_file)

    def test_when_init_db_is_called_it_should_create_table(self):
        try:
            with sqlite3.connect(self.db_file) as connection:
                cursor = connection.cursor()
                cursor.execute('SELECT * FROM contact')
        except sqlite3.OperationalError:
            self.fail("Should not have raised sqlite3.OperationalError")

    def test_when_add_is_called_it_should_return_an_autoincremented_id(self):

        contactNumber = self.contactDAO.add(
            Contact(1, "First", "Contact", "514-000-0000", "firstContact@polymtl.ca", 0, 2020))
        self.assertEqual(contactNumber, 1)

        contactNumber = self.contactDAO.add(
            Contact(2, "Second", "Contact", "514-000-0001", "secondtContact@polymtl.ca", 1, 2020))
        self.assertEqual(contactNumber, 2)

    def test_get_by_id_after_add_should_return_inserted_value(self):

        self.contactDAO.add(self.firstContact)
        self.contactDAO.add(self.secondContact)
        self.contactDAO.add(self.thirdContact)

        foundContact = self.contactDAO.get_by_id(2)
        self.assertTrue(areContactsEquals(foundContact, self.secondContact))

    def test_get_by_names_after_add_should_return_inserted_value(self):

        self.contactDAO.add(self.firstContact)
        self.contactDAO.add(self.secondContact)
        self.contactDAO.add(self.thirdContact)

        foundContact = self.contactDAO.get_by_names(self.firstContact.first_name, self.firstContact.last_name)
        self.assertTrue(areContactsEquals(foundContact, self.firstContact))

        self.assertEqual(self.contactDAO.get_by_names("fake", "contact"), None);

    def test_get_by_id_with_undefined_rowid_should_return_None(self):
        self.contactDAO.add(self.firstContact)
        self.contactDAO.add(self.secondContact)
        self.contactDAO.add(self.thirdContact)

        self.assertEqual(self.contactDAO.get_by_id(40), None)

    def test_get_by_names_with_notexisted_contact_should_return_None(self):
        self.contactDAO.add(self.firstContact)
        self.contactDAO.add(self.secondContact)
        self.contactDAO.add(self.thirdContact)

        self.assertEqual(self.contactDAO.get_by_names("fake", "contact"), None);

    def test_deactivate_contact_then_get_it_with_id_should_be_not_updated(self):
        self.contactDAO.add(self.firstContact)
        self.contactDAO.add(self.secondContact)
        self.contactDAO.add(self.thirdContact)

        self.contactDAO.add(self.fourthContact)
        self.contactDAO.add(self.fifthContact)
        self.contactDAO.add(self.sixthContact)

        self.contactDAO.deactivate(1)
        self.contactDAO.deactivate(2)
        self.contactDAO.deactivate(3)

        self.assertEqual(self.contactDAO.get_by_id(1).updated, 0)
        self.assertEqual(self.contactDAO.get_by_id(2).updated, 0)
        self.assertEqual(self.contactDAO.get_by_id(3).updated, 0)

        self.assertEqual(self.contactDAO.get_by_id(4).updated, 1)
        self.assertEqual(self.contactDAO.get_by_id(5).updated, 1)
        self.assertEqual(self.contactDAO.get_by_id(6).updated, 1)

    def test_deactivate_contact_on_undefined_id_should_return_zero(self):
        self.contactDAO.add(self.firstContact)
        self.contactDAO.add(self.secondContact)
        self.contactDAO.add(self.thirdContact)
        self.contactDAO.add(self.fifthContact)
        self.contactDAO.add(self.sixthContact)
        self.contactDAO.add(self.seventhContact)

        self.assertEqual(self.contactDAO.deactivate(20), 0)

    def test_after_deleting_contact_by_id_get_it_with_id_should_return_None(self):
        self.contactDAO.add(self.firstContact)
        self.contactDAO.add(self.secondContact)
        self.contactDAO.add(self.thirdContact)
        self.contactDAO.add(self.fifthContact)

        self.contactDAO.delete_by_id(4)

        self.assertEqual(self.contactDAO.get_by_id(4), None)

    def test_deleting_undefined_id_should_return_zero(self):
        self.contactDAO.add(self.firstContact)
        self.assertEqual(self.contactDAO.delete_by_id(4), 0)
        self.assertEqual(self.contactDAO.delete_by_id(-2), 0)

    def test_after_deleting_contact_by_names_get_item_with_id_should_return_None(self):
        self.contactDAO.add(self.firstContact)

        self.contactDAO.delete_by_names(self.firstContact.first_name, self.firstContact.last_name)

        self.assertEqual(self.contactDAO.get_by_names(self.firstContact.first_name, self.firstContact.last_name), None)

    def test_deleting_not_existed_contact_should_return_zero(self):
        self.assertEqual(self.contactDAO.delete_by_names("fake", "contact"), 0)

    def test_update_contact_should_set_the_provided_values(self):
        updatedContact = Contact(1, "Bob", "Contact", "514-000-0000", "bob@polymtl.ca", 1, 2020)

        self.contactDAO.add(self.firstContact)

        self.contactDAO.update(updatedContact)

        foundContact = self.contactDAO.get_by_id(1)
        self.assertTrue(foundContact, updatedContact)

    def test_update_contact_should_return_zero_if_id_does_not_exist(self):
        self.contactDAO.add(self.firstContact)
        self.contactDAO.add(self.secondContact)
        self.contactDAO.add(self.thirdContact)
        self.contactDAO.add(self.fifthContact)

        self.assertEqual(self.contactDAO.update(self.sixthContact), 0)
        self.assertEqual(self.contactDAO.update(self.eighthContact), 0)

    def test_list_contacts_with_no_contacts_added_returns_empty_list(self):
        self.assertEqual(self.contactDAO.list(), [])

    def test_list_contacts_with_one_contact_should_return_list_with_contact(self):
        self.contactDAO.add(self.firstContact)
        contactList = self.contactDAO.list()
        self.assertTrue(areContactsEquals(contactList[0], self.firstContact))

    def test_list_contacts_with_updated_False_and_all_items_updated_should_return_empty_list(self):
        self.contactDAO.add(Contact(1, "First", "Contact", "514-000-0000", "firstContact@polymtl.ca", 1, 2020))
        self.contactDAO.add(Contact(2, "second", "Contact", "514-000-0001", "secondContact@polymtl.ca", 1, 2020))
        self.contactDAO.add(Contact(3, "Third", "Contact", "514-000-0002", "thirdContact@polymtl.ca", 1, 2020))

        self.assertEqual(self.contactDAO.list(False), [])

    def test_list_contacts_with_updated_True_and_all_items_not_updated_should_return_empty_list(self):
        self.contactDAO.add(Contact(1, "First", "Contact", "514-000-0000", "firstContact@polymtl.ca", 0, 2020))
        self.contactDAO.add(Contact(2, "second", "Contact", "514-000-0001", "secondContact@polymtl.ca", 0, 2020))
        self.contactDAO.add(Contact(3, "Third", "Contact", "514-000-0002", "thirdContact@polymtl.ca", 0, 2020))

        self.assertEqual(self.contactDAO.list(True), [])

    def test_list_contacts_with_all_not_updated_items_and_updated_False_should_return_all_contacts(self):
        firstContact = Contact(1, "First", "Contact", "514-000-0000", "firstContact@polymtl.ca", 0, 2020)
        secondContact = Contact(2, "second", "Contact", "514-000-0001", "secondContact@polymtl.ca", 0, 2020)
        thirdContact = Contact(3, "Third", "Contact", "514-000-0002", "thirdContact@polymtl.ca", 0, 2020)

        self.contactDAO.add(firstContact)
        self.contactDAO.add(secondContact)
        self.contactDAO.add(thirdContact)

        contactList = self.contactDAO.list(False)
        self.assertEqual(len(contactList), 3)
        self.assertTrue(isConctatInList(contactList, firstContact))
        self.assertTrue(isConctatInList(contactList, secondContact))
        self.assertTrue(isConctatInList(contactList, thirdContact))

    def test_list_contacts_with_all_updated_items_and_updated_True_should_return_all_contacts(self):
        firstContact = Contact(1, "First", "Contact", "514-000-0000", "firstContact@polymtl.ca", 1, 2020)
        secondContact = Contact(2, "second", "Contact", "514-000-0001", "secondContact@polymtl.ca", 1, 2020)
        thirdContact = Contact(3, "Third", "Contact", "514-000-0002", "thirdContact@polymtl.ca", 1, 2020)

        self.contactDAO.add(firstContact)
        self.contactDAO.add(secondContact)
        self.contactDAO.add(thirdContact)

        contactList = self.contactDAO.list(True)
        self.assertEqual(len(contactList), 3)
        self.assertTrue(isConctatInList(contactList, firstContact))
        self.assertTrue(isConctatInList(contactList, secondContact))
        self.assertTrue(isConctatInList(contactList, thirdContact))


if __name__ == '__main__':
    unittest.main()