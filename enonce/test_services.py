import unittest
from unittest.mock import Mock
from models import Contact
from services import ContactService, AlreadyExistedItem, UndefinedID, NotExistedItem
from datetime import datetime

# To complete...
class TestContactService(unittest.TestCase):

    def setUp(self):
        self.contactDAO = Mock()
        self.contactService = ContactService(self.contactDAO)

    def test_when_contact_is_created_updated_should_be_True(self):
        self.contactDAO.add.return_value = 1
        self.contactDAO.get_by_names.return_value = None
        contact = self.contactService.create_contact('Houssem','Ben Braiek','123-456-7891','houssem.bb@gmail.com')
        self.assertTrue(contact.updated)
    
    def test_when_contact_is_created_updated_date_should_be_now(self):
        self.contactDAO.add.return_value = 1
        self.contactDAO.get_by_names.return_value = None
        contact = self.contactService.create_contact('Houssem', 'Ben Braiek', '123-456-7891', 'houssem.bb@gmail.com')
        self.assertAlmostEqual(contact.updated_date, datetime.now().timestamp(), 0)

    def test_when_contact_is_created_and_DAO_get_by_names_returns_contact_it_should_raise_AlreadyExistedItem(self):
        self.contactDAO.get_by_names.return_value = Contact(0, "firstname", "lastname","438-499-9274","test@polymtl.ca",True, datetime.now().timestamp())
        self.assertRaises(AlreadyExistedItem, self.contactService.create_contact, "Houssem", "Ben Braiek","123-456-7891", "houssem.bb@gmail.com")
    
    def test_when_contact_is_changed_updated_should_be_True(self):
        contact = self.contactService.update_contact(1,'Houssem', 'Ben Braiek', '123-456-7891', 'houssem.bb@gmail.com')
        self.contactDAO.update.return_value = 1
        self.assertTrue(contact.updated)


    def test_when_contact_is_changed_updated_date_should_be_now(self):
        pass
    
    def test_when_contact_is_changed_and_DAO_update_returns_zero_it_should_raise_UndefinedID(self):
        pass

    def test_when_retrieve_contact_is_called_with_id_and_DAO_get_by_id_should_be_called(self):
        pass
    
    def test_when_retrieve_contact_is_called_with_names_and_DAO_get_by_names_should_be_called(self):
        pass

    def test_when_retrieve_contact_is_called_with_id_and_DAO_returns_None_it_should_raise_UndefinedID(self):
        pass
    
    def test_when_retrieve_contact_is_called_with_names_and_DAO_returns_None_it_should_raise_NotExistedItem(self):
        pass

    def test_when_delete_contact_is_called_with_id_and_DAO_delete_by_id_should_be_called(self):
        pass
    
    def test_when_delete_contact_is_called_with_names_and_DAO_delete_by_names_should_be_called(self):
        pass

    def test_when_delete_contact_is_called_with_id_and_DAO_delete_by_id_returns_zero_it_should_raise_UndefinedID(self):
        pass
    
    def test_when_retrieve_contact_is_called_with_names_and_DAO_delete_by_names_returns_zero_it_should_raise_NotExistedItem(self):
        pass

    """Tests to verify the phone number"""
    def test_to_verify_we_got_a_valid_american_phone_number(self):
        self.assertTrue(self.contactService.check_phone('438-499-9274'))
        self.assertTrue(self.contactService.check_phone("4384999274"))
        self.assertTrue(self.contactService.check_phone("(438)-499-9274"))
        self.assertTrue(self.contactService.check_phone("(438)4999274"))
        self.assertTrue(self.contactService.check_phone("(438)499-9274"))

        self.assertFalse(self.contactService.check_phone('438499927'))
        self.assertFalse(self.contactService.check_phone("43849992756"))
        self.assertFalse(self.contactService.check_phone("438-499-92756"))
        self.assertFalse(self.contactService.check_phone("43-499-9275"))
        self.assertFalse(self.contactService.check_phone("(438)-499-927"))
        self.assertFalse(self.contactService.check_phone("(438)-499-92745"))
        self.assertFalse(self.contactService.check_phone("(438)499927"))
        self.assertFalse(self.contactService.check_phone("(438)49992746"))
        self.assertFalse(self.contactService.check_phone("(4389)499-9274"))
        self.assertFalse(self.contactService.check_phone("(43)499-9274"))

        self.assertFalse(self.contactService.check_phone('438-49@9-9274'))
        self.assertFalse(self.contactService.check_phone("#4384999274"))
        self.assertFalse(self.contactService.check_phone("(438)-499-9274&"))
        self.assertFalse(self.contactService.check_phone("(438)499^9274"))
        self.assertFalse(self.contactService.check_phone("(438.)499-9274"))


    """Tests  to verify the email address"""
    def test_to_verify_we_got_a_valid_email_address(self):
        emails = ["yahoo","ymail","gmail", "hotmail", "videotron","polymtl"]
        self.assertTrue(self.contactService.check_mail("hello@yahoo.com"))
        self.assertTrue(self.contactService.check_mail("hello.world@ymail.ca"))

        self.assertFalse(self.contactService.check_mail("test.polymtl.com"))
        self.assertFalse(self.contactService.check_mail("abc@videotron.xxx"))
        self.assertFalse(self.contactService.check_mail("hello@hotmail"))
        self.assertFalse(self.contactService.check_mail("@ymail.fr"))
        self.assertFalse(self.contactService.check_mail("test@yahoo,ca"))

    
if __name__ == '__main__':
    unittest.main()
    